# ITExpert - Template Method Pattern
Ceci est le code source utilisé dans mon article sur les patrons de méthode (Template Method Pattern).

[Lien vers l'article](https://itexpert.fr/blog/patron-methode)

### Prérequis
- Installer les SDK de .NET 5

### Guide d'utilisation
1. Cloner le code source.
2. Ouvrir la racine du répertoire dans une invite de commandes.
3. Éxecuter la commande "dotnet run" pour lancer le programme.

