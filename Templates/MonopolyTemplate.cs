using System;

namespace ITExpertTemplateMethod.Templates
{
    public class MonopolyTemplate
    {
        public MonopolyTemplate() { }

        public void Play()
        {
            PlayerAction();
            LoosingCondition();
        }

        protected virtual void PlayerAction() =>
            Console.Write("Throw dices, move, buy and pay. ");

        protected virtual void LoosingCondition() =>
            Console.Write("Go bankrupt. ");
    }
}