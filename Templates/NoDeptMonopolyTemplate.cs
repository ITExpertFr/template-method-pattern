using System;

namespace ITExpertTemplateMethod.Templates
{
    public class NoDeptMonopolyTemplate : MonopolyTemplate
    {
        public NoDeptMonopolyTemplate() { }

        protected override void LoosingCondition() =>
            Console.Write("Loosing all your cash. ");
    }
}