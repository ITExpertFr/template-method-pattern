using System;

namespace ITExpertTemplateMethod.Templates
{
    public class CheaterEditionMonopolyTemplate : MonopolyTemplate
    {
        public CheaterEditionMonopolyTemplate() { }

        protected override void PlayerAction() =>
            Console.Write("Throw dices, move, buy, pay and cheat a lot! ");
    }
}