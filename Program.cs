﻿using System;
using ITExpertTemplateMethod.Templates;

namespace ITExpertTemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            MonopolyTemplate monopolyWithoutDept = new NoDeptMonopolyTemplate();
            monopolyWithoutDept.Play();     // Throw dices, move, buy and pay. Loosing all your cash. 

            MonopolyTemplate monopolyCheaterEdition = new CheaterEditionMonopolyTemplate();
            monopolyCheaterEdition.Play();  // Throw dices, move, buy, pay and cheat a lot! Go bankrupt. 

            MonopolyTemplate classicMonopoly = new MonopolyTemplate();
            classicMonopoly.Play();         // Throw dices, move, buy and pay. Go bankrupt. 
        }
    }
}
